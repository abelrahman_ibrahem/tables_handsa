package com.bb.schedule;

import java.util.ArrayList;




import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.AdapterView.OnItemClickListener;



public class Years  extends ListActivity{
	Context context;
	private ListView lvyears;
	private ActivityList listAdapter;
	static String department;
@Override
protected void onPause() {
	// TODO Auto-generated method stub
	finish();
	super.onPause();
}
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_main);
		context = this;
		
		init_components();
		
	}

	

	private void init_components() {
		//bsignin = (Button) findViewById(R.id.bsignin);
		lvyears =getListView();
		
		Bundle b1=getIntent().getExtras();
		department =b1.getString("department");
		
		String[] years ;
		
	
			years= getResources().getStringArray(R.array.Year);
	
		ArrayList<String> depts = new ArrayList<String>();
		for (int i = 0; i < years.length; i++) {
			depts.add(years[i]);
		}
		listAdapter = new ActivityList(this, android.R.id.text1, depts);
		
		setListAdapter(listAdapter);
		lvyears.setTextFilterEnabled(true);
		lvyears.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				
				Intent in = new Intent(Years.this,Schedule.class);
				Bundle b=new Bundle();
				b.putString("department",department);
				b.putString("year", Integer.toString(position));
				//Toast.makeText(context, "t"+year+"_"+Integer.toString(position) , Toast.LENGTH_SHORT).show();
				in.putExtras(b);
				startActivity(in);
			}
		});
		lvyears.setBackgroundResource(R.drawable.background);
	/*	bsignin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(context, "Signin", Toast.LENGTH_SHORT).show();
				// TODO Auto-generated method stub
				bsignin_click(v);
			}
		});
*/
	}

	

	class ActivityList extends ArrayAdapter<String> {

		public ActivityList(Context context, int textViewResourceId,
				ArrayList<String> objects) {
			super(context, textViewResourceId, objects);
		}
		//private int[] images={R.drawable.cs,R.drawable.is,R.drawable.it};

		
		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater
						.from(Years.this);
				convertView = inflater
						.inflate(R.layout.listitem, parent, false);
				holder = new ViewHolder();

		holder.ivlogo = (ImageView) convertView
						.findViewById(R.id.ivlogo);
				holder.tvhead = (TextView) convertView
						.findViewById(R.id.tvmain);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
//			if(year.equals("2")||year.equals("3")){
//			holder.ivlogo.setImageBitmap(BitmapFactory.decodeResource(
//					getResources(), images[position]));}
//			else
		holder.ivlogo.setVisibility(0);
		
			
			
			//holder.tvhead.setText(getResources().getStringArray(R.array.Year)[position]);
			holder.tvhead.setText(getItem(position));
			return convertView;
		}

	}

	static class ViewHolder {
		TextView tvhead;
	
		ImageView ivlogo;
	}

}


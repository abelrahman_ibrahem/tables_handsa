package com.bb.schedule;



import android.app.Activity;
import android.content.Context;
import android.content.res.AssetManager;

import android.os.Bundle;
import android.view.Window;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class Schedule extends Activity  {

	//private ImageView myimage;
	private WebView myimage;
	String department,year;
	String prepa;
	Context context;
	
	 
		private  Thread alo;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
	
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		
		myimage=new WebView(this);
		setContentView(myimage);
	
		
		context=this;
		
		init_components();
	}
	private void init_components(){
		
		Bundle b=getIntent().getExtras();
		department =b.getString("department");
		year =b.getString("year");

		String table="t"+department+"_"+year+".png";
		
		String path ="file:///android_asset/t"+department+"_"+year+".png";
		AssetManager mg = getResources().getAssets();
		
		try {
		 mg.open(table);		  
		  WebViewClient clnt=new WebViewClient();
			myimage.setWebViewClient(clnt);
			//myimage.getSettings().setJavaScriptEnabled(true);
			//myimage.getSettings().setLoadWithOverviewMode(true);
			myimage.getSettings().setUseWideViewPort(true);
			myimage.getSettings().setBuiltInZoomControls(true);
			
			
			myimage.loadUrl(path);
				
				
			
		} catch (Exception ex) {
			ex.printStackTrace();
			Toast.makeText(this, "Sorry Your table is not up yet", Toast.LENGTH_LONG).show();
			finish();
		}
		
	
			
	}
	public Thread getAlo() {
		return alo;
	}
	protected void onPause() {
		// TODO Auto-generated method stub
		finish();
		super.onPause();
	}


		 
}    



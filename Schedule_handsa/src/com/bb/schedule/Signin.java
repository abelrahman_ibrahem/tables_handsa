package com.bb.schedule;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;

import android.widget.Spinner;

public class Signin extends Activity implements OnClickListener{

	private Spinner SpDept,Spyear;
//	private EditText etid;
//	private Button bsubmit;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		// TODO Auto-generated method stub
		super.onCreate(savedInstanceState);
		setContentView(R.layout.signin);
		init_components();
	}
	private void init_components(){
		SpDept=(Spinner)findViewById(R.id.spdept);
		Spyear=(Spinner)findViewById(R.id.spyear);
		//etid=(EditText)findViewById(R.id.etId);
		
		ArrayAdapter<CharSequence> deptsource=ArrayAdapter.createFromResource(this,R.array.Department,android.R.layout.simple_spinner_item);
		ArrayAdapter<CharSequence> yearsource=ArrayAdapter.createFromResource(this,R.array.Year, android.R.layout.simple_spinner_item);
		
		deptsource.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    SpDept.setAdapter(deptsource);
	    
	    yearsource.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
	    Spyear.setAdapter(yearsource);
		
	    SpDept.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				SpDept_selectedItemChanged(arg0,arg1,arg2,arg3);
				
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	    Spyear.setOnItemSelectedListener(new OnItemSelectedListener() {

			@Override
			public void onItemSelected(AdapterView<?> arg0, View arg1,
					int arg2, long arg3) {
				// TODO Auto-generated method stub
				Spyear_selectedItemChanged(arg0,arg1,arg2,arg3);
			}

			@Override
			public void onNothingSelected(AdapterView<?> arg0) {
				// TODO Auto-generated method stub
				
			}
		});
	    
	}
	
	private void bsubmit_click(View v){
		
		
	}
	private void SpDept_selectedItemChanged(AdapterView<?>av,View v,int pos,long id){
		if(pos==0){
			Spyear.setEnabled(false);			
		}else{
			Spyear.setEnabled(true);
			
		}
		
	}
	
	private void Spyear_selectedItemChanged(AdapterView<?>av,View v,int pos,long id){
		
		
	}
	@Override
	public void onClick(View v) {
		// TODO Auto-generated method stub
		if(v.getId()==R.id.bsubmit){
			bsubmit_click(v);
			
		}
	}
	

}

package com.bb.schedule;
/*
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.sql.SQLClientInfoException;

import android.R.integer;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteDatabase.CursorFactory;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Environment;
import android.util.Log;
*//*
public class DbManage {
	
	public static final String KEY_ROWID="id";
	public static final String KEY_app="app_name";
	public static final String KEY_rate="app_rate";
	
	private static final String DATABASE_NAME="Ratedb";
	public static final String DATABASE_Table="RateTable";
	private static final int DATABASE_Version=1;
	
	private DbHelper myhelper;
	private final Context ourContext;
	private SQLiteDatabase database;
	
	private static class DbHelper extends SQLiteOpenHelper{
		Context ourContext;
		public DbHelper(Context context) {
			
			super(context, DATABASE_NAME, null, DATABASE_Version);
			ourContext=context;
			// TODO Auto-generated constructor stub
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			// TODO Auto-generated method stub
			
			db.execSQL("Create Table "+DATABASE_Table+" (" +
					KEY_ROWID+" Integer primary key autoincrement, "+
					KEY_app+" text NOT NULL,"+
					KEY_rate+" float);"
					);
			ContentValues cv=new ContentValues();
			cv.put(KEY_app, ourContext.getString(R.string.app1));
			cv.put(KEY_rate, 0);
			db.insert(DATABASE_Table, null, cv);
			
			cv=new ContentValues();
			cv.put(KEY_app, ourContext.getString(R.string.app2));
			cv.put(KEY_rate, 0);
			db.insert(DATABASE_Table, null, cv);
			
			cv=new ContentValues();
			cv.put(KEY_app, ourContext.getString(R.string.app3));
			cv.put(KEY_rate, 0);
			db.insert(DATABASE_Table, null, cv);
			
			cv=new ContentValues();
			cv.put(KEY_app, ourContext.getString(R.string.app4));
			cv.put(KEY_rate, 0);
			db.insert(DATABASE_Table, null, cv);
			
			cv=new ContentValues();
			cv.put(KEY_app, ourContext.getString(R.string.app5));
			cv.put(KEY_rate, 0);
			db.insert(DATABASE_Table, null, cv);
			
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
			// TODO Auto-generated method stub
			
		}
		
	}
	
	public DbManage(Context c){
		ourContext=c;
		
	}
	public DbManage open(){
		myhelper=new DbHelper(ourContext);
		database=myhelper.getWritableDatabase();
		return this;
		
	}
	public void close(){
		myhelper.close();
		
	}

	public void rate(float[] r,String cmnt) {
		float temp=0;
		
		Cursor c=database.query(DATABASE_Table, columns, null, null, null, null, null);
		
		int iapp=c.getColumnIndex(KEY_app);
		int irate=c.getColumnIndex(KEY_rate);
		int iid=c.getColumnIndex(KEY_ROWID);
		
	for(int i=0;i<r.length-1;i++){
		if(r[i]!=0){
			//read the old value
			
			c.moveToFirst();
			c.move(i);
			
			temp=c.getFloat(irate);
			
			//write the new value
			
			

			
			database.execSQL("update "+DATABASE_Table+" set "+KEY_rate+"= "+Float.toString(temp+r[i])+" where "+KEY_ROWID+" = "+ Integer.toString(i+1));
			
		}
		c.moveToFirst();
		c.move(4);
		
		
		
		//write the new value
		
		

		
		database.execSQL("update "+DATABASE_Table+" set "+KEY_app+"= '"+c.getString(iapp)+cmnt+"\n"+"' where "+KEY_ROWID+" = "+ 5);
		
	}
		
	}
	String[] columns=new String[]{KEY_ROWID,KEY_app,KEY_rate};
	public String getdata() {
		// TODO Auto-generated method stub
		
		Cursor c=database.query(DATABASE_Table, columns, null, null, null, null, null);
		String result="";
		int iapp=c.getColumnIndex(KEY_app);
		int irate=c.getColumnIndex(KEY_rate);
		int iid=c.getColumnIndex(KEY_ROWID);
		
		for(c.moveToFirst();!c.isAfterLast();c.moveToNext()){
			try{
			result+=c.getInt(iid)+","+c.getString(iapp)+","+Float.toString( c.getFloat(irate))+"\n";
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		return result;
	}
	
	
}
*/

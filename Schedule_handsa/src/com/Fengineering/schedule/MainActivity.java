package com.Fengineering.schedule;

import java.util.ArrayList;






import com.Fengineering.schedule.R;

import android.os.Bundle;
import android.app.ListActivity;
import android.content.Context;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;


public class MainActivity extends ListActivity {
	//private Button bsignin;
	Context context;
	private ListView lvDepts;
	private ActivityList listAdapter;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		//setContentView(R.layout.activity_main);
		context = this;
		init_components();
	}

//	@Override
//	public boolean onCreateOptionsMenu(Menu menu) {
//		// Inflate the menu; this adds items to the action bar if it is present.
//		getMenuInflater().inflate(R.menu.main, menu);
//		return true;
//	}

	private void init_components() {
		//bsignin = (Button) findViewById(R.id.bsignin);
		
		
		lvDepts =getListView();

		String[] years = getResources().getStringArray(R.array.Department);
		ArrayList<String> year = new ArrayList<String>();
		for (int i = 0; i < years.length; i++) {
			year.add(years[i]);
		}
		listAdapter = new ActivityList(this, android.R.id.text1, year);

		

		setListAdapter(listAdapter);
		lvDepts.setTextFilterEnabled(true);
		lvDepts.setOnItemClickListener(new OnItemClickListener() {
			public void onItemClick(AdapterView<?> parent, View view,
					int position, long id) {
				if(getResources().getStringArray(R.array.Hasone)[position].equals("no")){
					Intent in = new Intent(MainActivity.this,Years.class);// Class.forName("com.example.android.supportv13.Data"));
					Bundle b=new Bundle();
					b.putString("department",Integer.toString( position) );
					in.putExtras(b);
					startActivity(in);
				}
				else if(getResources().getStringArray(R.array.Hasone)[position].equals("yes")){
					Intent in = new Intent(MainActivity.this,Schedule.class);// Class.forName("com.example.android.supportv13.Data"));
					Bundle b=new Bundle();
					b.putString("department", Integer.toString(position));
					b.putString("year","0");
					in.putExtras(b);
					startActivity(in);
					
				}
			}
		});
		lvDepts.setBackgroundColor(Color.YELLOW);
	/*	bsignin.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				Toast.makeText(context, "Signin", Toast.LENGTH_SHORT).show();
				// TODO Auto-generated method stub
				bsignin_click(v);
			}
		});
*/
	}

//	private void bsignin_click(View v) {
//
//		try {
//			Class oc = Class.forName("com.bb.schedule.Signin");
//			startActivity(new Intent(this, oc));
//		} catch (ClassNotFoundException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//
//	}

	class ActivityList extends ArrayAdapter<String> {
		int[] logos = {R.drawable.pre,R.drawable.computer1,R.drawable.kahrba,
				R.drawable.etisalat,R.drawable.mech1,
				R.drawable.entag,R.drawable.entag1,R.drawable.madanay,R.drawable.emara,R.drawable.emara,R.drawable.emara}
				;

		public ActivityList(Context context, int textViewResourceId,
				ArrayList<String> objects) {
			super(context, textViewResourceId, objects);
		}

		@Override
		public View getView(int position, View convertView, ViewGroup parent) {
			ViewHolder holder;
			if (convertView == null) {
				LayoutInflater inflater = LayoutInflater
						.from(MainActivity.this);
				convertView = inflater
						.inflate(R.layout.listitem, parent, false);
				holder = new ViewHolder();

				holder.ivlogo = (ImageView) convertView
						.findViewById(R.id.ivlogo);
//				holder.tvdescription = (TextView) convertView
//						.findViewById(R.id.tvdescpription);
				holder.tvhead = (TextView) convertView
						.findViewById(R.id.tvmain);
				convertView.setTag(holder);
			} else {
				holder = (ViewHolder) convertView.getTag();
			}
			holder.ivlogo.setImageBitmap(BitmapFactory.decodeResource(
					getResources(), logos[position]));

//			holder.tvdescription.setText(getItem(position));

		//	holder.tvhead.setText(getResources().getStringArray(R.array.Department)[position]);
			holder.tvhead.setText(getItem(position));
			return convertView;
		}

	}

	static class ViewHolder {
		TextView tvhead;
//		TextView tvdescription;
		ImageView ivlogo;
	}

}
